/*
 * motion.c
 *
 *  Created on: 2013/03/18
 *      Author: yuichi
 */

#include "motion.h"

#include "ics20.hpp"

#include "leg.hpp"
#include <Eigen/Core>

using namespace Eigen;

extern Leg legs[8];

void Motion_SetStretchAll(uint8_t str) {
	for (int i = 0; i < 24; i++) {
		gICS.setStretch(i, str);
	}
}

void Motion_SetSpeedAll(uint8_t spd) {
	for (int i = 0; i < 24; i++) {
		gICS.setSpeed(i, spd);
	}
}

void Motion_FreeAll(){
	for(int i=0;i<24;i++){
		gICS.setPos(i,0);
	}
}

extern "C" void Motion_RefreshLegs(){
	for(int i=0;i<8;i++){
		legs[i].refreshPos();
	}
}

void Motion_EasingMoveLegs(Vector3f inits[8],Vector3f dests[8],int T,int Tl){
	for (int t = 0; t < T; t++) {
		for(int i=0;i<8;i++){
			Vector3f dest = vectoreasing(inits[i], dests[i], T, Tl, t);
			legs[i].setPos(dest, false);
		}
	}
}
