/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_it.h"
#include "stm32f4xx_conf.h"

#include "usart.h"

extern uint8_t USART_RxBuffer[USART_NBUF][64];
extern uint8_t USART_RxBufPos;
extern uint8_t USART_RxBufLinePos;

extern uint8_t USART_TxBuffer[USART_NBUF][128];
extern uint8_t USART_TxReadLinePos;
extern uint8_t USART_TxBufLinePos;

/******************************************************************************/
/*            Cortex-M4 Processor Exceptions Handlers                         */
/******************************************************************************/

void WWDG_IRQHandler(void) {
	USART_Puts("WWDG\r\n");
}

/**
 * @brief   This function handles NMI exception.
 * @param  None
 * @retval None
 */
void NMI_Handler(void) {
	/* This interrupt is generated when HSE clock fails */

	if (RCC_GetITStatus(RCC_IT_CSS ) != RESET) {
		/* At this stage: HSE, PLL are disabled (but no change on PLL config) and HSI
		 is selected as system clock source */

		/* Enable HSE */
		RCC_HSEConfig(RCC_HSE_ON );

		/* Enable HSE Ready and PLL Ready interrupts */
		RCC_ITConfig(RCC_IT_HSERDY | RCC_IT_PLLRDY, ENABLE);

		/* Clear Clock Security System interrupt pending bit */
		RCC_ClearITPendingBit(RCC_IT_CSS );

		/* Once HSE clock recover, the HSERDY interrupt is generated and in the RCC ISR
		 routine the system clock will be reconfigured to its previous state (before
		 HSE clock failure) */
	}
}

/**
 * @brief  This function handles Hard Fault exception.
 * @param  None
 * @retval None
 */
void HardFault_Handler(void) {
	USART_Puts("HardFault\n");
	/* Go to infinite loop when Hard Fault exception occurs */
	while (1) {
	}
}

/**
 * @brief  This function handles Memory Manage exception.
 * @param  None
 * @retval None
 */
void MemManage_Handler(void) {
	/* Go to infinite loop when Memory Manage exception occurs */
	while (1) {
	}
}

/**
 * @brief  This function handles Bus Fault exception.
 * @param  None
 * @retval None
 */
void BusFault_Handler(void) {
	/* Go to infinite loop when Bus Fault exception occurs */
	while (1) {
	}
}

/**
 * @brief  This function handles Usage Fault exception.
 * @param  None
 * @retval None
 */
void UsageFault_Handler(void) {
	/* Go to infinite loop when Usage Fault exception occurs */
	while (1) {
	}
}

/**
 * @brief  This function handles SVCall exception.
 * @param  None
 * @retval None
 */
void SVC_Handler(void) {
}

/**
 * @brief  This function handles Debug Monitor exception.
 * @param  None
 * @retval None
 */
void DebugMon_Handler(void) {
}

/**
 * @brief  This function handles PendSVC exception.
 * @param  None
 * @retval None
 */
void PendSV_Handler(void) {
}

/**
 * @brief  This function handles SysTick Handler.
 * @param  None
 * @retval None
 */
void SysTick_Handler(void) {
}

/******************************************************************************/
/*                 STM32F4xx Peripherals Interrupt Handlers                   */
/*  Add here the Interrupt Handler for the used peripheral(s) (PPP), for the  */
/*  available peripheral interrupt handler's name please refer to the startup */
/*  file (startup_stm32f4xx.s).                                               */
/******************************************************************************/

/**
 * @brief  This function handles PPP interrupt request.
 * @param  None
 * @retval None
 */
/*void PPP_IRQHandler(void)
 {
 }*/

/**
 * @brief  This function handles RCC interrupt request.
 * @param  None
 * @retval None
 */
void RCC_IRQHandler(void) {
	if (RCC_GetITStatus(RCC_IT_HSERDY ) != RESET) {
		/* Clear HSERDY interrupt pending bit */
		RCC_ClearITPendingBit(RCC_IT_HSERDY );

		/* Check if the HSE clock is still available */
		if (RCC_GetFlagStatus(RCC_FLAG_HSERDY ) != RESET) {
			/* Enable PLL: once the PLL is ready the PLLRDY interrupt is generated */
			RCC_PLLCmd(ENABLE);
		}
	}

	if (RCC_GetITStatus(RCC_IT_PLLRDY ) != RESET) {
		/* Clear PLLRDY interrupt pending bit */
		RCC_ClearITPendingBit(RCC_IT_PLLRDY );

		/* Check if the PLL is still locked */
		if (RCC_GetFlagStatus(RCC_FLAG_PLLRDY ) != RESET) {
			/* Select PLL as system clock source */
			RCC_SYSCLKConfig(RCC_SYSCLKSource_PLLCLK );
		}
	}
}

void USART3_IRQHandler(void) {
	if (USART_GetITStatus(USART3, USART_IT_RXNE ) != RESET) {
		uint8_t ch = USART_ReceiveData(USART3 ) & 0xFF;
		if (ch == 0x08) {
			if (USART_RxBufPos == 0)
				return;
			USART_RxBuffer[USART_RxBufLinePos][--USART_RxBufPos] = 0;
			USART_Puts("\b \b");
			return;
		} else if (ch == 0x7f) {
			return;
		}
		USART_RxBuffer[USART_RxBufLinePos][USART_RxBufPos++] = ch;
		USART_RxBufPos &= 0x3F;
		USART_Putc(ch);
		if (ch == '\r') {
			USART_Putc('\n');
			USART_RxBufLinePos++;
			USART_RxBufLinePos &= 31;
			USART_RxBufPos = 0;
		}
		USART_ClearITPendingBit(USART3, USART_IT_RXNE );
	}
}

void DMA1_Stream3_IRQHandler(void) {
	DMA1->LIFCR|=DMA_LIFCR_CFEIF3;
	if (DMA1->LISR|DMA_LISR_TCIF3) {
	//if(DMA_GetITStatus(DMA1_Stream3,DMA_IT_TCIF4)){
		DMA_ClearITPendingBit(DMA1_Stream3,DMA_IT_HTIF4|DMA_IT_TCIF4);
		//DMA1->LIFCR|=DMA_LIFCR_CHTIF3|DMA_LIFCR_CTCIF3|DMA_LIFCR_CTEIF3|DMA_LIFCR_CFEIF3;
		USART_PushTxBuffer();
	}
	DMA_ClearITPendingBit(DMA1_Stream3,DMA_IT_HTIF4|DMA_IT_TCIF4|DMA_IT_FEIF4);
}
