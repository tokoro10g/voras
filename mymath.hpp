/*
 * mymath.hpp
 *
 *  Created on: 2013/03/10
 *      Author: yuichi
 */

#ifndef MYMATH_HPP_
#define MYMATH_HPP_

#include <stdint.h>

#include <Eigen/Core>
#include <Eigen/Dense>

using namespace Eigen;

static const float PI = 3.14159265f;
static const uint8_t TABLESIZE = 128; // suppose to be 2^n
static const uint8_t SUBBIT = 8;
static const uint16_t SUBINDEX = (1 << SUBBIT);
static const uint32_t I_PI = (TABLESIZE * SUBINDEX * 2);
static const uint16_t I_HPI = (TABLESIZE * SUBINDEX);

int sign(float x);
float sin(float x);
float cos(float x);
float fabs(float x);
int abs(int x);
float hypot(float a, float b);

float easing(float T,float Tl,float t);
Vector3f vectoreasing(Vector3f start,Vector3f end,float T,float Tl,float t);

Matrix4f Rx(float theta);
Matrix4f Ry(float theta);
Matrix4f Rz(float theta);
Matrix4f Rv(float theta, uint8_t axis);
Matrix4f RxDot(float theta);
Matrix4f RyDot(float theta);
Matrix4f RzDot(float theta);
Matrix4f Tv(Vector3f d);

float sqNorm(Vector3f v);

Vector3i rad2srvAng(Vector3f ang, Vector3i center, int dir);
Vector3f srvAng2rad(Vector3i srv, Vector3i center, int dir);
Vector3f normalizeAngVec(Vector3f v);

#endif /* MYMATH_HPP_ */
