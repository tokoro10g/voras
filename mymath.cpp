/*
 * mymath.cpp
 *
 *  Created on: 2013/03/10
 *      Author: yuichi
 */

#include <Eigen/Core>
#include <Eigen/Dense>

#include "mymath.hpp"
#include "ik.hpp"

using namespace Eigen;

//int(sin(index / (pi / 2) * 128) * 65535 + 0.5)
const unsigned short sin_table[] = { 0, 804, 1608, 2412, 3216, 4019, 4821,
		5623, 6424, 7223, 8022, 8820, 9616, 10411, 11204, 11996, 12785, 13573,
		14359, 15142, 15924, 16703, 17479, 18253, 19024, 19792, 20557, 21319,
		22078, 22834, 23586, 24334, 25079, 25820, 26557, 27291, 28020, 28745,
		29465, 30181, 30893, 31600, 32302, 32999, 33692, 34379, 35061, 35738,
		36409, 37075, 37736, 38390, 39039, 39682, 40319, 40950, 41575, 42194,
		42806, 43411, 44011, 44603, 45189, 45768, 46340, 46905, 47464, 48014,
		48558, 49095, 49624, 50145, 50659, 51166, 51664, 52155, 52638, 53113,
		53580, 54039, 54490, 54933, 55367, 55794, 56211, 56620, 57021, 57413,
		57797, 58171, 58537, 58895, 59243, 59582, 59913, 60234, 60546, 60850,
		61144, 61429, 61704, 61970, 62227, 62475, 62713, 62942, 63161, 63371,
		63571, 63762, 63943, 64114, 64276, 64428, 64570, 64703, 64826, 64939,
		65042, 65136, 65219, 65293, 65357, 65412, 65456, 65491, 65515, 65530,
		65535, 0 };

int sign(float x) {
	return ((x > 0) * 2 - (x != 0));
}

float sin(float x) {
	long ix, subix, sign, tval;

	ix = (int) (x * (I_PI / PI));
	sign = ix & I_PI;
	ix &= (I_PI - 1);
	if (ix > I_HPI)
		ix = I_PI - ix;

	subix = ix & (SUBINDEX - 1);
	ix >>= SUBBIT;

	tval = ((long) sin_table[ix] * (SUBINDEX - subix)
			+ (long) sin_table[ix + 1] * subix);

	return (sign ? -tval : tval) / (SUBINDEX * 65535.f);
}

float cos(float x) {
	return sin(x + PI / 2.f);
}

float fabs(float x) {
	return (x >= 0) ? x : -x;
}

int abs(int x) {
	return (x >= 0) ? x : -x;
}

float hypot(float a, float b) {
	a = fabs(a);
	b = fabs(b);
	if (a < b) {
		float c = a;
		a = b;
		b = c;
	}
	if (b == 0)
		return a;
	const uint8_t ITERATION_NUMBER = 3;
	float s;
	for (uint8_t i = 0; i < ITERATION_NUMBER; i++) {
		s = (b / a) * (b / a);
		s /= 4.0f + s;
		a += 2.0f * a * s;
		b *= s;
	}
	return a;
}

float easing(float T,float Tl,float t){
	float ts=(T-Tl)/2.f;
	if(t<0){
		return 0.f;
	} else if(t<ts){
		return (2.f/(T*T-Tl*Tl))*t*t;
	} else if(t<ts+Tl){
		return 2.f*t/(T+Tl)-(T-Tl)/2.f/(T+Tl);
	} else if(t<T){
		return 1.f-2.f*(t-T)*(t-T)/(T*T-Tl*Tl);
	} else {
		return 1.f;
	}
}

Vector3f vectoreasing(Vector3f start,Vector3f end,float T,float Tl,float t){
	float e=easing(T,Tl,t);
	return end*e+start*(1.f-e);
}

Matrix4f Rx(float theta) {
	Matrix4f M = Matrix4f::Identity();
	M(1, 1) = cos(theta);
	M(1, 2) = -sin(theta);
	M(2, 1) = sin(theta);
	M(2, 2) = cos(theta);
	return M;
}

Matrix4f Ry(float theta) {
	Matrix4f M = Matrix4f::Identity();
	M(0, 0) = cos(theta);
	M(0, 2) = sin(theta);
	M(2, 0) = -sin(theta);
	M(2, 2) = cos(theta);
	return M;
}

Matrix4f Rz(float theta) {
	Matrix4f M = Matrix4f::Identity();
	M(0, 0) = cos(theta);
	M(0, 1) = -sin(theta);
	M(1, 0) = sin(theta);
	M(1, 1) = cos(theta);
	return M;
}

Matrix4f Rv(float theta, uint8_t axis) {
	switch (axis) {
	case AxisX:
		return Rx(theta);
		break;
	case AxisY:
		return Ry(theta);
		break;
	case AxisZ:
		return Rz(theta);
		break;
	default:
		return Matrix4f::Zero();
	}
}

Matrix4f RxDot(float theta) {
	Matrix4f M = Matrix4f::Zero();
	M(1, 1) = -sin(theta);
	M(1, 2) = -cos(theta);
	M(2, 1) = cos(theta);
	M(2, 2) = -sin(theta);
	return M;
}

Matrix4f RyDot(float theta) {
	Matrix4f M = Matrix4f::Zero();
	M(0, 0) = -sin(theta);
	M(0, 2) = cos(theta);
	M(2, 0) = -cos(theta);
	M(2, 2) = -sin(theta);
	return M;
}

Matrix4f RzDot(float theta) {
	Matrix4f M = Matrix4f::Zero();
	M(0, 0) = -sin(theta);
	M(0, 1) = -cos(theta);
	M(1, 0) = cos(theta);
	M(1, 1) = -sin(theta);
	return M;
}

Matrix4f Tv(Vector3f d) {
	Matrix4f M = Matrix4f::Identity();
	M(0, 3) = d(0);
	M(1, 3) = d(1);
	M(2, 3) = d(2);
	return M;
}

float sqNorm(Vector3f v) {
	float a = hypot((float)v(0), (float)v(1));
	return hypot(a * a, (float)v(2));
}

Vector3i rad2srvAng(Vector3f ang, Vector3i center, int dir) {
	Vector3i srv = center;
	ang(0) *= (float) dir;
	srv += (ang * 180.f / PI / 0.034f).cast<int> ();
	return srv;
}

Vector3f srvAng2rad(Vector3i srv, Vector3i center, int dir) {
	Vector3f ang = (srv - center).cast<float> ();
	ang(0) *= (float) dir;
	ang *= 0.034f * PI / 180.f;
	return ang;
}

Vector3f normalizeAngVec(Vector3f v) {
	for (int i = 0; i < 3; i++) {
		while (v(i) >= PI) {
			v(i) -= 2.f * PI;
		}
		while (v(i) < -PI) {
			v(i) += 2.f * PI;
		}
	}
	return v;
}
