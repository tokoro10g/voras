#include "stm32f4xx.h"
#include "stm32f4xx_conf.h"
#include "ics20.hpp"
#include "motion.h"
#include "usart.h"
#include "mymath.hpp"
#include "leg.hpp"

#include "ik.hpp"

#include "cmd.hpp"

#include <cstdio>
#include <Eigen/Core>
#include <Eigen/Geometry>
#include <Eigen/Dense>

using namespace std;
using namespace Eigen;

volatile uint32_t Count = 0xFFFFF;
uint8_t EEP_Data[58];

void USART_ChangeBaudrate(uint32_t baudrate);
void Delay(__IO uint32_t nCount);

Leg legs[8];

uint32_t wait=0x4FFFF;

int main(void) {

	legs[0]=Leg(0, Vector3i(6200, 9230, 7750),Vector3f(100.f, 240.f, -50.f));
	legs[1]=Leg(1, Vector3i(6200, 9100, 7730),Vector3f(-100.f, 240.f, -50.f));
	legs[2]=Leg(2, Vector3i(6850, 9000, 7900),Vector3f(240.f, 50.f, -50.f));
	legs[3]=Leg(3, Vector3i(6850, 9000, 7900),Vector3f(-240.f, 50.f, -50.f));
	legs[4]=Leg(4, Vector3i(6850, 9000, 7900),Vector3f(240.f, -50.f, -50.f));
	legs[5]=Leg(5, Vector3i(6850, 9000, 7900),Vector3f(-240.f, -50.f, -50.f));
	legs[6]=Leg(6, Vector3i(8750, 9000, 8000),Vector3f(100.f, -240.f, -50.f));
	legs[7]=Leg(7, Vector3i(8750, 9000, 8000),Vector3f(-100.f, -240.f, -50.f));
	SystemInit();
	gICS.init();

	//BoardInit(); // Configure board specific setting

	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);

	USART_Config(9600);
	USART_ChangeBaudrate(115200);

	Motion_SetStretchAll(127);
	Motion_SetSpeedAll(80);

	while (1) /* Toggle LED */
	{
		Cmd_Proc();
		Delay(Count);
	}
	return 0;
}

void USART_ChangeBaudrate(uint32_t baudrate){
	USART_printf("AT+UART=%d,0,0",baudrate);
	USART_ITConfig(USART3, USART_IT_RXNE, DISABLE);
	Delay(65535);
	USART_Cmd(USART3,DISABLE);
	USART_DeInit(USART3);
	USART_Config(921600);
}

void Delay(__IO uint32_t nCount) {
	for (; nCount != 0; nCount--)
		;
}
