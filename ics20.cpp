/*
 * ics20.c
 *
 *  Created on: 2013/03/17
 *      Author: yuichi
 */

#include "stm32f4xx.h"
#include "stm32f4xx_conf.h"

#include "ics20.hpp"
#include "spi.hpp"

ICS gICS;

typedef struct {
	GPIO_TypeDef *GPIO;
	uint32_t Pin;
} PortPin;

static const PortPin PortPinMap[8]={
	{GPIOB,GPIO_Pin_13},
	{GPIOB,GPIO_Pin_14},
	{GPIOB,GPIO_Pin_15},
	{GPIOC,GPIO_Pin_6},
	{GPIOC,GPIO_Pin_7},
	{GPIOC,GPIO_Pin_8},
	{GPIOC,GPIO_Pin_9},
	{GPIOA,GPIO_Pin_8}
};

void ICS::init() {
	gSPI.init();
	for (int i = 0; i < 8; i++) {
		GPIO_SetBits(PortPinMap[i].GPIO, PortPinMap[i].Pin);
	}

	TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStruct;
	TIM_TimeBaseInitStruct.TIM_ClockDivision = TIM_CKD_DIV2;
	TIM_TimeBaseInitStruct.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInitStruct.TIM_Period = 1400;
	TIM_TimeBaseInitStruct.TIM_Prescaler = 71;
	TIM_TimeBaseInit(TIM4, &TIM_TimeBaseInitStruct);
	TIM_ITConfig(TIM4, TIM_IT_Update, ENABLE);

	TIM_Cmd(TIM4, ENABLE);

	NVIC_InitTypeDef NVIC_InitStruct;
	NVIC_InitStruct.NVIC_IRQChannel = TIM4_IRQn;
	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0;
	NVIC_Init(&NVIC_InitStruct);
}

void ICS::addCmd(uint8_t cmd, uint8_t id, uint32_t subcmd) {
	criticalSection = 1;
	cmdBuffer[cmdBufPos].cmd = cmd;
	cmdBuffer[cmdBufPos].id = id;
	cmdBuffer[cmdBufPos].subcmd = subcmd;
	criticalSection = 0;

	cmdBufPos++;
	cmdBufPos &= 0x7F;
}

void ICS::setPos(uint8_t id, uint16_t pos) {
	addCmd(ICS_CMD_POS, id, ((pos & 0x3F80) << 9) | ((pos & 0x7F) << 8));
}

void ICS::setStretch(uint8_t id, uint8_t str) {
	if (str > 127)
		str = 127;
	addCmd(ICS_CMD_PRW, id, (ICS_PR_SC_STR << 16) | str << 8);
}

void ICS::setSpeed(uint8_t id, uint8_t spd) {
	if (spd > 127)
		spd = 127;
	addCmd(ICS_CMD_PRW, id, (ICS_PR_SC_SPD << 16) | spd << 8);
}

void ICS::setParamAndPos(uint8_t id, uint8_t str, uint8_t spd, uint16_t pos) {
	setStretch(id, str);
	setSpeed(id, spd);
	setPos(id, pos);
}

void ICS::timerProc() {
	if(criticalSection) return;

	uint8_t cmd = cmdBuffer[cmdReadPos].cmd;
	uint8_t id = cmdBuffer[cmdReadPos].id;
	uint32_t subcmd = cmdBuffer[cmdReadPos].subcmd;
	uint8_t cmdwithid = (cmd << 5) | id;

	switch (cmd) {
	case ICS_CMD_NOP:
		return;
		break;
	case ICS_CMD_POS:
		txBuffer[0]=cmdwithid;
		txBuffer[1]=(subcmd & 0xFF0000) >> 16;
		txBuffer[2]=(subcmd & 0x00FF00) >> 8;
		break;
	case ICS_CMD_PRR:
		if ((subcmd & 0x30000) == (ICS_PR_SC_EEP << 16)) {
			// EEP Read
			return;
		} else {
			txBuffer[0]=(cmdwithid);
			txBuffer[1]=(subcmd & 0x030000) >> 16;
			txBuffer[2]=((subcmd & 0x007F00) >> 8);
		}
		break;
	case ICS_CMD_PRW:
		if ((subcmd & 0x30000) == (ICS_PR_SC_EEP << 16)) {
			// EEP Write
			return;
		} else {
			txBuffer[0]=(cmdwithid);
			txBuffer[1]=((subcmd & 0x030000) >> 16);
			txBuffer[2]=((subcmd & 0x007F00) >> 8);
		}
		break;
	case ICS_CMD_ID:
		/*
		ICS1_TxBuffer[0]=(cmdwithid);
		ICS1_TxBuffer[1]=((subcmd & 0xFF0000) >> 16);
		ICS1_TxBuffer[2]=((subcmd & 0x00FF00) >> 8);
		ICS1_TxBuffer[3]=(subcmd & 0x0000FF);
		*/
		break;
	}

	GPIO_ResetBits(PortPinMap[id/3].GPIO, PortPinMap[id/3].Pin);
	gSPI.sendByte(txBuffer[0]);
	gSPI.sendByte(txBuffer[1]);
	gSPI.sendByte(txBuffer[2]);
	GPIO_SetBits(PortPinMap[id/3].GPIO, PortPinMap[id/3].Pin);
	/*
	 * send data here
	 */

	cmdBuffer[cmdReadPos].cmd = ICS_CMD_NOP;

	cmdReadPos++;
	cmdReadPos &= 0x7F;
}

void ICS_Proc(){
	gICS.timerProc();
}
