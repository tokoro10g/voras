/*
 * ik.hpp
 *
 *  Created on: 2013/03/30
 *      Author: yuichi
 */

#ifndef IK_HPP_
#define IK_HPP_

#include <stdint.h>

#include "usart.h"
#include "mymath.hpp"

#include <Eigen/Core>
#include <Eigen/Dense>

using namespace Eigen;

static const uint8_t NotTh = 0;
static const uint8_t IsTh = 1;
static const uint8_t Offset = 0;
static const uint8_t Rotate = 1;
static const uint8_t AxisX = 0;
static const uint8_t AxisY = 1;
static const uint8_t AxisZ = 2;

typedef struct {
	unsigned isTh:1;
	unsigned type:1;
	unsigned axis:2;
	float value;
} LegTrans;

static const LegTrans Trans_Dat[8][8]={
	{
		// fr
		{NotTh,Offset,AxisX,49.4f},
		{NotTh,Offset,AxisY,88.6f},
		{NotTh,Rotate,AxisZ,55.f*PI/180.f},
		{IsTh,Rotate,AxisZ,1.f},
		{NotTh,Offset,AxisX,20.9f},
		{NotTh,Rotate,AxisZ,45.f*PI/180.f},
		{NotTh,Offset,AxisX,31.4f},
		{NotTh,Rotate,AxisX,15.f*PI/180.f}
	},
	{
		// fl
		{NotTh,Offset,AxisX,-49.4f},
		{NotTh,Offset,AxisY,88.6f},
		{NotTh,Rotate,AxisZ,125.f*PI/180.f},
		{IsTh,Rotate,AxisZ,1.f},
		{NotTh,Offset,AxisX,20.9f},
		{NotTh,Rotate,AxisZ,-45.f*PI/180.f},
		{NotTh,Offset,AxisX,31.4f},
		{NotTh,Rotate,AxisX,-15.f*PI/180.f}
	},
	{
		// mfr
		{NotTh,Offset,AxisX,56.6f},
		{NotTh,Offset,AxisY,27.2f},
		{NotTh,Rotate,AxisZ,15.f*PI/180.f},
		{IsTh,Rotate,AxisZ,1.f},
		{NotTh,Offset,AxisX,40.5f},
		{NotTh,Offset,AxisX,0.f},
		{NotTh,Offset,AxisX,0.f},
		{NotTh,Offset,AxisX,0.f},
	},
	{
		// mfl
		{NotTh,Offset,AxisX,-56.6f},
		{NotTh,Offset,AxisY,27.2f},
		{NotTh,Rotate,AxisZ,165.f*PI/180.f},
		{IsTh,Rotate,AxisZ,1.f},
		{NotTh,Offset,AxisX,40.5f},
		{NotTh,Offset,AxisX,0.f},
		{NotTh,Offset,AxisX,0.f},
		{NotTh,Offset,AxisX,0.f},
	},
	{
		// mbr
		{NotTh,Offset,AxisX,56.6f},
		{NotTh,Offset,AxisY,-27.2f},
		{NotTh,Rotate,AxisZ,-15.f*PI/180.f},
		{IsTh,Rotate,AxisZ,1.f},
		{NotTh,Offset,AxisX,40.5f},
		{NotTh,Offset,AxisX,0.f},
		{NotTh,Offset,AxisX,0.f},
		{NotTh,Offset,AxisX,0.f},
	},
	{
		// mbl
		{NotTh,Offset,AxisX,-56.6f},
		{NotTh,Offset,AxisY,-27.2f},
		{NotTh,Rotate,AxisZ,-165.f*PI/180.f},
		{IsTh,Rotate,AxisZ,1.f},
		{NotTh,Offset,AxisX,40.5f},
		{NotTh,Offset,AxisX,0.f},
		{NotTh,Offset,AxisX,0.f},
		{NotTh,Offset,AxisX,0.f},
	},
	{
		// br
		{NotTh,Offset,AxisX,49.4f},
		{NotTh,Offset,AxisY,-88.6f},
		{NotTh,Rotate,AxisZ,-55.f*PI/180.f},
		{IsTh,Rotate,AxisZ,1.f},
		{NotTh,Offset,AxisX,20.9f},
		{NotTh,Rotate,AxisZ,-45.f*PI/180.f},
		{NotTh,Offset,AxisX,31.4f},
		{NotTh,Offset,AxisX,0.f},
	},
	{
		// bl
		{NotTh,Offset,AxisX,-49.4f},
		{NotTh,Offset,AxisY,-88.6f},
		{NotTh,Rotate,AxisZ,-125.f*PI/180.f},
		{IsTh,Rotate,AxisZ,1.f},
		{NotTh,Offset,AxisX,20.9f},
		{NotTh,Rotate,AxisZ,45.f*PI/180.f},
		{NotTh,Offset,AxisX,31.4f},
		{NotTh,Offset,AxisX,0.f},
	}
};
static const LegTrans Trans_Dat_Common[6]={
	{NotTh,Offset,AxisZ,35.8f},
	{IsTh,Rotate,AxisY,1.0f},
	{NotTh,Offset,AxisX,53.5f},
	{IsTh,Rotate,AxisY,1.0f},
	{NotTh,Rotate,AxisY,49.2f*PI/180.f},
	{NotTh,Offset,AxisX,134.1f}
};

float rangePotential(float thetabegin, float thetaend, float theta0);
Vector3f T(uint8_t legid, Vector3f theta);
Matrix3f JInv(uint8_t legid, Vector3f theta);
const Vector3f Newton(uint8_t legid, Vector3f theta0, Vector3f dest);
const Vector3f Newton_NextIter(uint8_t legid, Vector3f theta0, Vector3f dest);

#endif /* IK_HPP_ */
