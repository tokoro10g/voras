/*
 * usart.h
 *
 *  Created on: 2013/02/20
 *      Author: yuichi
 */

#ifndef USART_H_
#define USART_H_

#include "stm32f4xx.h"
#ifdef __cplusplus
 extern "C" {
#endif
#define USART_NBUF 32

#define NUMARGS(...)  (sizeof((int[]){__VA_ARGS__})/sizeof(int))
#define USART_printf(format, ...)  (_USART_printf(format, NUMARGS(__VA_ARGS__), __VA_ARGS__))
#define USART_scanf(format, ...)  (_USART_scanf(format, NUMARGS(__VA_ARGS__), __VA_ARGS__))

void USART_DMAConfig();
void USART_Config(uint32_t baudrate);
void USART_Gets(char *buf);
void USART_Putc(uint8_t byte);
void USART_Puts(const char *str);
void USART_WriteByteByHex(uint8_t byte);
void USART_WriteByteByDigit(uint8_t byte);
void USART_WriteHalfWordByHex(uint16_t hword);
void USART_WriteHalfWordByDigit(uint16_t hword);
void USART_WriteWordByHex(uint32_t word);
void USART_WriteWordByDigit(uint32_t word);
void USART_WriteNewLine(void);
uint8_t USART_GetChrCmd();
void USART_PushTxBuffer();
int _USART_scanf(const char* format, int narg, ...);
void _USART_printf(const char* format, int narg, ...);

#ifdef __cplusplus
 }
#endif
#endif /* USART_H_ */
