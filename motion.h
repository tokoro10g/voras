/*
 * motion.h
 *
 *  Created on: 2013/03/18
 *      Author: yuichi
 */

#ifndef MOTION_H_
#define MOTION_H_
#include "ics20.hpp"
#include <Eigen/Core>
using namespace Eigen;

#define SRV_FR1 0
#define SRV_FR2 1
#define SRV_FR3 2
#define SRV_FL1 3
#define SRV_FL2 4
#define SRV_FL3 5
#define SRV_MFR1 6
#define SRV_MFR2 7
#define SRV_MFR3 8
#define SRV_MFL1 9
#define SRV_MFL2 10
#define SRV_MFL3 11
#define SRV_MBR1 12
#define SRV_MBR2 13
#define SRV_MBR3 14
#define SRV_MBL1 15
#define SRV_MBL2 16
#define SRV_MBL3 17
#define SRV_BR1 18
#define SRV_BR2 19
#define SRV_BR3 20
#define SRV_BL1 21
#define SRV_BL2 22
#define SRV_BL3 23

void Motion_SetStretchAll(uint8_t str);
void Motion_SetSpeedAll(uint8_t spd);
void Motion_FreeAll();
void Motion_EasingMoveLegs(Vector3f inits[8],Vector3f dests[8],int T,int Tl);
extern "C" void Motion_RefreshLegs();

#endif /* MOTION_H_ */
