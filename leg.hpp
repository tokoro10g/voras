/*
 * leg.hpp
 *
 *  Created on: 2013/04/04
 *      Author: yuichi
 */

#ifndef LEG_HPP_
#define LEG_HPP_

#include <stdint.h>
#include "ik.hpp"

#include <Eigen/Core>
#include <Eigen/Dense>

#include "usart.h"

using namespace Eigen;

class Leg{
private:
	uint8_t id;
	Vector3i center;
	Vector3f theta;
	Vector3f dest;
	Vector3f cur;
	Vector3f old;
	uint8_t speed;
	uint8_t step;
	uint8_t curstep;
	uint8_t linear;
public:
	Leg(){};
	Leg(uint8_t _id, Vector3i _center, Vector3f _dest):
		id(_id),
		center(_center),
		dest(_dest),
		cur(_dest),
		old(_dest),
		speed(150),
		step(10),
		curstep(0),
		linear(5)
	{
		theta=Newton(_id,Vector3f(0,0,0),_dest);
	}
	~Leg(){};
	void setPos(Vector3f _pos,bool spflag=false);
	void setDest(Vector3f dest,int step,int linear);
	void refreshPos();
	const Vector3f getPos() const;
	const Vector3f getTheta() const;
	void setSrvSpeed(Vector3i speed) const;
	void setZeroPos() const;
};

#endif /* LEG_HPP_ */
