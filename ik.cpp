/*
 * ik.cpp
 *
 *  Created on: 2013/03/30
 *      Author: yuichi
 */

#include "ik.hpp"
#include "mymath.hpp"
#include "usart.h"

float rangePotential(float thetabegin, float thetaend, float theta0){
	if(theta0<thetabegin){
		return -10;
	} else if(theta0>thetaend){
		return 10;
	} else {
		return 0;
	}
}

Vector3f T(uint8_t legid, Vector3f theta){
	if(legid>=8) return Vector3f::Zero();

	Matrix4f t=Matrix4f::Identity();

	for(int i=0;i<8;i++){
		LegTrans tr=Trans_Dat[legid][i];
		if(tr.value==0.f) break;
		if(tr.type==Rotate){
			float ang=tr.value;
			if(tr.isTh==IsTh){
				ang=theta(0);
			}
			t*=Rv(ang,tr.axis);
		} else {
			Vector3f v=Vector3f::Zero();
			v(tr.axis)=tr.value;
			t*=Tv(v);
		}
	}

	int j=1;
	for(int i=0;i<6;i++){
		LegTrans tr=Trans_Dat_Common[i];
		if(tr.type==Rotate){
			float ang=tr.value;
			if(tr.isTh==IsTh){
				ang=theta(j);
				j++;
			}
			t*=Rv(ang,tr.axis);
		} else {
			Vector3f v=Vector3f::Zero();
			v(tr.axis)=tr.value;
			t*=Tv(v);
		}
	}

	return t.block(0,3,3,1);
}

Matrix3f JInv(uint8_t legid,Vector3f theta){
	if(legid>=8) return Matrix3f::Zero();

	Matrix3f M=Matrix3f::Zero();

	Matrix4f t;

	for(int i=0;i<3;i++){
		t=Matrix4f::Identity();
		for(int j=0;j<8;j++){
			LegTrans tr=Trans_Dat[legid][j];
			if(tr.value==0.f) break;
			if(tr.type==Rotate){
				float ang=tr.value;
				if(tr.isTh==IsTh){
					ang=theta(0);
				}
				if(tr.isTh==IsTh&&i==0){
					t*=RzDot(ang);
				} else {
					t*=Rv(ang,tr.axis);
				}
			} else {
				Vector3f v=Vector3f::Zero();
				v(tr.axis)=tr.value;
				t*=Tv(v);
			}
		}

		int k=1;
		for(int j=0;j<6;j++){
			LegTrans tr=Trans_Dat_Common[j];
			if(tr.type==Rotate){
				float ang=tr.value;
				if(tr.isTh==IsTh){
					ang=theta(k);
					k++;
				}
				if(tr.isTh==IsTh&&i!=0&&i==k-1){
					t*=RyDot(ang);
				} else {
					t*=Rv(ang,tr.axis);
				}
			} else {
				Vector3f v=Vector3f::Zero();
				v(tr.axis)=tr.value;
				t*=Tv(v);
			}
		}
		M.block(0,i,3,1)=t.block(0,3,3,1);
	}

	return M.inverse();
}

const Vector3f Newton(uint8_t legid, Vector3f theta0, Vector3f dest){
	Vector3f theta0_bk=theta0;
	Vector3f theta;
	for(int i=0;i<100;i++){
		theta=Newton_NextIter(legid,theta0,dest);
		if((theta-theta0).norm()<0.07f){
			return theta;
		}
		theta0=theta;
	}
	return theta0_bk;
}

const Vector3f Newton_NextIter(uint8_t legid, Vector3f theta0, Vector3f dest){
	Vector3f theta;
	theta=theta0+JInv(legid,theta0)*(dest-T(legid,theta0))*0.6f;
	Vector3f p(
			rangePotential(-1.78f,1.78f,(float)theta0(0)),
			rangePotential(-2.2f,1.8f,(float)theta0(1)),
			rangePotential(-0.3f,1.9f,(float)theta0(2))
		  );
	theta-=p*0.05f;
	theta=normalizeAngVec(theta);
	return theta;
}
