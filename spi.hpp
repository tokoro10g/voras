/*
 * spi.hpp
 *
 *  Created on: 2013/09/01
 *      Author: yuichi
 */

#include "stm32f4xx.h"
#include "stm32f4xx_conf.h"

#ifndef SPI_HPP_
#define SPI_HPP_

#ifdef __cplusplus
extern "C"{
#endif

class SPI {
private:

public:
	void init();
	void sendByte(uint8_t byte);
};

extern SPI gSPI;

#ifdef __cplusplus
}
#endif

#endif /* SPI_HPP_ */
