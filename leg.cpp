/*
 * leg.cpp
 *
 *  Created on: 2013/04/02
 *      Author: yuichi
 */

#include <stdint.h>

#include "mymath.hpp"
#include "leg.hpp"
#include "ik.hpp"
#include "ics20.hpp"
#include "motion.h"
#include "usart.h"

void Leg::setPos(Vector3f pos,bool spflag){
	old=cur;
	dest=pos;
	cur=pos;
	curstep=step;
	theta = Newton(id,theta,cur);
	Vector3i srv = rad2srvAng(theta, center, (id % 2) * 2 - 1);
	for (uint8_t i = 0; i < 3; i++){
		gICS.setPos(id * 3 + i, (uint16_t) srv(i));
	}
}

void Leg::setDest(Vector3f _dest,int _step,int _linear) {
	old=cur;
	dest=_dest;
	step=_step;
	linear=_linear;
	curstep=0;
}

void Leg::refreshPos(){
	if(step==curstep) return;
	cur = vectoreasing(old, dest, step, linear, ++curstep);
	theta = Newton(id,theta,cur);
	Vector3i srv = rad2srvAng(theta, center, (id % 2) * 2 - 1);
	for (uint8_t i = 0; i < 3; i++){
		gICS.setPos(id * 3 + i, (uint16_t) srv(i));
	}
}

void Leg::setSrvSpeed(Vector3i speed) const {
	for (uint8_t i = 0; i < 3; i++) {
		gICS.setSpeed(id * 3 + i, (uint8_t)speed(i));
	}
}

void Leg::setZeroPos() const{
	Vector3i srv = rad2srvAng(Vector3f(0,0,0), center, (id % 2) * 2 - 1);
	for (uint8_t i = 0; i < 3; i++) {
		gICS.setSpeed(id * 3 + i, 80);
		gICS.setPos(id * 3 + i, (uint16_t)srv(i));
	}
}

const Vector3f Leg::getPos() const {
	return T(id,theta);
}

const Vector3f Leg::getTheta() const{
	return theta;
}
