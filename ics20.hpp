/*
 * ics20.h
 *
 *  Created on: 2013/03/17
 *      Author: yuichi
 */

#ifndef ICS20_H_
#define ICS20_H_

#include "stm32f4xx.h"
#include "stm32f4xx_conf.h"

#ifdef __cplusplus
extern "C"{
#endif

#define ICS_CMD_NOP 0b000
#define ICS_CMD_POS 0b100
#define ICS_CMD_PRR 0b101
#define ICS_CMD_PRW 0b110
#define ICS_CMD_ID 0b111

#define ICS_PR_SC_EEP 0b00
#define ICS_PR_SC_STR 0b01
#define ICS_PR_SC_SPD 0b10

typedef struct {
	unsigned cmd :3;
	unsigned id :5;
	unsigned subcmd :24;
} ICSCommand;

class ICS{
private:
	bool status;
	bool criticalSection;
	uint8_t txBuffer[3];
	ICSCommand cmdBuffer[128];
	uint8_t cmdBufPos;
	uint8_t cmdReadPos;
public:
	void init();
	void enable(bool state);
	void addCmd(uint8_t cmd, uint8_t id, uint32_t subcmd);
	void setPos(uint8_t id,uint16_t pos);
	void setStretch(uint8_t id,uint8_t str);
	void setSpeed(uint8_t id,uint8_t spd);
	void setParamAndPos(uint8_t id,uint8_t str,uint8_t spd,uint16_t pos);
	void timerProc();
};

extern ICS gICS;

void ICS_Proc();

#ifdef __cplusplus
}
#endif

#endif /* ICS20_H_ */
