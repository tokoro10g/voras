/*
 * cmd.cpp
 *
 *  Created on: 2013/04/12
 *      Author: yuichi
 */

#include "stdint.h"
#include "usart.h"

#include "mymath.hpp"
#include "motion.h"
#include "leg.hpp"
#include "ics20.hpp"

#include <Baselibc/stdio.h>

extern Leg legs[8];

static float Height_Down = -60.f;
static float Height_Middle = Height_Down + 10.f;
static float Height_Up = Height_Down + 20.f;

signed char moveDir = 1;
uint8_t speed = 100;

void Cmd_Proc() {
	uint8_t USARTCmd = USART_GetChrCmd();
	char strbuf[64];
	switch (USARTCmd) {
	case '1':
	{
		Vector3f legdests[8];
		legdests[0]=Vector3f(100.f, 240.f, Height_Middle);
		legdests[1]=Vector3f(-100.f, 265.f, Height_Down);
		legdests[2]=Vector3f(200.f, 105.f, Height_Down);
		legdests[3]=Vector3f(-200.f, 80.f, Height_Up);
		legdests[4]=Vector3f(200.f, -80.f, Height_Up);
		legdests[5]=Vector3f(-200.f, -40.f, Height_Down);
		legdests[6]=Vector3f(100.f, -220.f, Height_Down - 10.f);
		legdests[7]=Vector3f(-100.f, -240.f, Height_Middle - 10.f);
		for(int i=0;i<8;i++){
			legs[i].setDest(legdests[i],20,2);
		}
	}
		break;
	case '2':
	{
		Vector3f legdests[8];
		legdests[0]=Vector3f(100.f, 240.f, Height_Middle);
		legdests[1]=Vector3f(-100.f, 220.f, Height_Down);
		legdests[2]=Vector3f(200.f, 40.f, Height_Down);
		legdests[3]=Vector3f(-200.f, 100.f, Height_Middle);
		legdests[4]=Vector3f(200.f, -60.f, Height_Middle);
		legdests[5]=Vector3f(-200.f, -105.f, Height_Down);
		legdests[6]=Vector3f(100.f, -265.f, Height_Down - 10.f);
		legdests[7]=Vector3f(-100.f, -240.f, Height_Middle - 10.f);
		for(int i=0;i<8;i++){
			legs[i].setDest(legdests[i],20,2);
		}
	}
		break;
	case '3':
	{
		Vector3f legdests[8];
		legdests[0]=Vector3f(100.f, 265.f, Height_Down);
		legdests[1]=Vector3f(-100.f, 240.f, Height_Middle);
		legdests[2]=Vector3f(200.f, 80.f, Height_Up);
		legdests[3]=Vector3f(-200.f, 105.f, Height_Down);
		legdests[4]=Vector3f(200.f, -40.f, Height_Down);
		legdests[5]=Vector3f(-200.f, -80.f, Height_Up);
		legdests[6]=Vector3f(100.f, -240.f, Height_Middle - 10.f);
		legdests[7]=Vector3f(-100.f, -220.f, Height_Down - 10.f);
		for(int i=0;i<8;i++){
			legs[i].setDest(legdests[i],20,2);
		}
	}
		break;
	case '4':
	{
		Vector3f legdests[8];
		legdests[0]=Vector3f(100.f, 220.f, Height_Down);
		legdests[1]=Vector3f(-100.f, 240.f, Height_Middle);
		legdests[2]=Vector3f(200.f, 100.f, Height_Middle);
		legdests[3]=Vector3f(-200.f, 40.f, Height_Down);
		legdests[4]=Vector3f(200.f, -105.f, Height_Down);
		legdests[5]=Vector3f(-200.f, -60.f, Height_Middle);
		legdests[6]=Vector3f(100.f, -240.f, Height_Middle - 10.f);
		legdests[7]=Vector3f(-100.f, -265.f, Height_Down - 10.f);
		for(int i=0;i<8;i++){
			legs[i].setDest(legdests[i],20,2);
		}
	}
		break;
	case 's':
		legs[0].setPos(Vector3f(120.f, 220.f, 0.f));
		legs[1].setPos(Vector3f(-120.f, 220.f, 0.f));
		legs[2].setPos(Vector3f(200.f, 50.f, 0.f));
		legs[3].setPos(Vector3f(-200.f, 50.f, 0.f));
		legs[4].setPos(Vector3f(200.f, -50.f, 0.f));
		legs[5].setPos(Vector3f(-200.f, -50.f, 0.f));
		legs[6].setPos(Vector3f(120.f, -220.f, 0.f));
		legs[7].setPos(Vector3f(-120.f, -220.f, 0.f));
		break;
	case 'n':
		legs[0].setPos(Vector3f(100.f, 220.f, Height_Down));
		legs[1].setPos(Vector3f(-100.f, 220.f, Height_Down));
		legs[2].setPos(Vector3f(200.f, 50.f, Height_Down));
		legs[3].setPos(Vector3f(-200.f, 50.f, Height_Down));
		legs[4].setPos(Vector3f(200.f, -50.f, Height_Down));
		legs[5].setPos(Vector3f(-200.f, -50.f, Height_Down));
		legs[6].setPos(Vector3f(100.f, -220.f, Height_Down));
		legs[7].setPos(Vector3f(-100.f, -220.f, Height_Down));
		break;
	case 't':
		for (int i = 0; i < 8; i++) {
			Vector3f lth = legs[i].getTheta();
			sprintf(strbuf, "Leg %d: %.2f,%.2f,%.2f\r\n", i, lth(0), lth(1),
					lth(2));
			USART_Puts(strbuf);
		}
		break;
	case 'p':
		for (int i = 0; i < 8; i++) {
			Vector3f lpos = legs[i].getPos();
			sprintf(strbuf, "Leg %d: %.2f,%.2f,%.2f\r\n", i, lpos(0), lpos(1),
					lpos(2));
			USART_Puts(strbuf);
		}
		break;
	case '0':
		for (uint8_t i = 0; i < 8; i++)
			legs[i].setZeroPos();
		break;
	case 'f':
		Motion_FreeAll();
		break;
	case 'b':
		moveDir = -moveDir;
		USARTCmd = '4';
		break;
	case 'm':
	{
		uint8_t id;
		USART_Puts("id?\r\n");
		USART_Gets(strbuf);
		if (sscanf(strbuf, "%d", &id) != 1) {
			USART_Puts("Invalid value.\r\n");
			break;
		}
		if (id > 7) {
			USART_Puts("Invalid value.\r\n");
			break;
		}
		USART_Puts("x,y,z?\r\n");
		USART_Gets(strbuf);
		int x, y, z;
		if (sscanf(strbuf, "%d,%d,%d", &x, &y, &z) != 3) {
			USART_Puts("Invalid value.\r\n");
			break;
		}
		legs[id].setPos(Vector3f(x, y, z));
		sprintf(strbuf, "Leg %d has been set to %d,%d,%d\r\n", id, x, y, z);
		USART_Puts(strbuf);
	}
		break;
	case ',':
		for (int i = 0; i < 8; i++) {
			Vector3f lpos = legs[i].getPos();
			lpos = Ry(-0.2).block(0, 0, 3, 3) * lpos;
			legs[i].setDest(lpos,10,5);
		}
		break;
	case '.':
		for (int i = 0; i < 8; i++) {
			Vector3f lpos = legs[i].getPos();
			lpos = Ry(0.2).block(0, 0, 3, 3) * lpos;
			legs[i].setDest(lpos,10,5);
		}
		break;
	case '/':
		for (int i = 0; i < 8; i++) {
			Vector3f lpos = legs[i].getPos();
			lpos = Rx(-0.2).block(0, 0, 3, 3) * lpos;
			legs[i].setDest(lpos,10,5);
		}
		break;
	case '\\':
		for (int i = 0; i < 8; i++) {
			Vector3f lpos = legs[i].getPos();
			lpos = Rx(0.2).block(0, 0, 3, 3) * lpos;
			legs[i].setDest(lpos,10,5);
		}
		break;
	case ';':
		for (int i = 0; i < 8; i++) {
			Vector3f lpos = legs[i].getPos();
			lpos = Rz(0.2).block(0, 0, 3, 3) * lpos;
			legs[i].setDest(lpos,10,5);
		}
		break;
	case ':':
		for (int i = 0; i < 8; i++) {
			Vector3f lpos = legs[i].getPos();
			lpos = Rz(-0.2).block(0, 0, 3, 3) * lpos;
			legs[i].setDest(lpos,10,5);
		}
		break;
	case 'g':
		for (int t = 0; t < 30; t++) {
			for (int i = 0; i < 8; i++) {
				Vector3f lpos = legs[i].getPos();
				lpos = Rz(-0.01).block(0, 0, 3, 3) * lpos;
				legs[i].setPos(lpos,false);
			}
		}
		break;
	case '^':
		for (int t = 0; t < 15; t++) {
			for (int i = 0; i < 8; i++) {
				Vector3f lpos = legs[i].getPos();
				lpos = Rx(0.03*cos(2.f*PI*(float)t/15.f)).block(0, 0, 3, 3) * lpos;
				lpos = Ry(0.03*sin(2.f*PI*(float)t/15.f)).block(0, 0, 3, 3) * lpos;
				legs[i].setPos(lpos,false);
			}
		}
		break;
	case '[':
		speed += 10;
		sprintf(strbuf, "Set Speed:%d\r\n", speed);
		USART_Puts(strbuf);
		for (uint8_t i = 0; i < 8; i++)
			legs[i].setSrvSpeed(Vector3i((int)speed,(int)speed,(int)speed));
		break;
	case ']':
		speed -= 10;
		sprintf(strbuf, "Set Speed:%d\r\n", speed);
		USART_Puts(strbuf);
		for (uint8_t i = 0; i < 8; i++)
			legs[i].setSrvSpeed(Vector3i((int)speed,(int)speed,(int)speed));
		break;
	case '{':
		Height_Down -= 10.f;
		Height_Middle -= 10.f;
		Height_Up -= 10.f;
		sprintf(strbuf, "Set Height:%f\r\n", Height_Down);
		USART_Puts(strbuf);
		for(int i=0;i<8;i++){
			legs[i].setDest(legs[i].getPos()-Vector3f(0,0,10.f),5,3);
		}
		break;
	case '}':
		Height_Down += 10.f;
		Height_Middle += 10.f;
		Height_Up += 10.f;
		sprintf(strbuf, "Set Height:%f\r\n", Height_Down);
		USART_Puts(strbuf);
		for(int i=0;i<8;i++){
			legs[i].setDest(legs[i].getPos()+Vector3f(0,0,10.f),5,3);
		}
		break;
	case 'a':
		{
			uint8_t id;
			USART_Puts("id?\r\n");
			USART_Gets(strbuf);
			if (sscanf(strbuf, "%d", &id) != 1) {
				USART_Puts("Invalid value.\r\n");
				break;
			}
			if (id > 7) {
				USART_Puts("Invalid value.\r\n");
				break;
			}

			USART_Puts("T,Tl?\r\n");
			USART_Gets(strbuf);
			int T, Tl;
			if (sscanf(strbuf, "%d,%d", &T, &Tl) != 2) {
				USART_Puts("Invalid value.\r\n");
				break;
			}

			USART_Puts("x,y,z?\r\n");
			USART_Gets(strbuf);
			int x, y, z;
			if (sscanf(strbuf, "%d,%d,%d", &x, &y, &z) != 3) {
				USART_Puts("Invalid value.\r\n");
				break;
			}
			Vector3f fdest = Vector3f((float) x, (float) y, (float) z);
			legs[id].setDest(fdest,T,Tl);
		}
		break;
	default:
		break;
	}
	/*
	 if (USARTCmd <= '4' && USARTCmd >= '1') {
	 USARTCmd += moveDir;
	 if (USARTCmd == '5') {
	 USARTCmd = '1';
	 }
	 if (USARTCmd == '0') {
	 USARTCmd = '4';
	 }
	 }
	 if (USARTCmd > '4' || USARTCmd < '1') {
	 USARTCmd = '\0';
	 }
	 */
}
