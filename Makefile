SHELL = /bin/sh
TARGET_ARCH   = -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 
INCLUDE_DIRS  = -I ../../Libraries \
				-I ../../Libraries/STM32F4xx_StdPeriph_Driver/inc \
				-I ../../Libraries/CMSIS/Device/ST/STM32F4xx/Include \
				-I ../../Libraries/CMSIS/Include \
				-I $(TOOLDIR)../arm-none-eabi/include \
				-I $(TOOLDIR)../arm-none-eabi/include/c++/4.8.1
STARTUP_DIR = ../../Libraries/CMSIS/Device/ST/STM32F4xx/Source/Templates/gcc_ride7/
BOARD_OPTS = -DHSE_VALUE=\(\(uint32_t\)8000000\) -DSTM32F4XX
FIRMWARE_OPTS = -DUSE_STDPERIPH_DRIVER
COMPILE_OPTS  = -Os -g3 -ffunction-sections -fpermissive -fdata-sections -fsigned-char -fno-rtti -fexceptions -Wall -fmessage-length=0 $(INCLUDE_DIRS) $(BOARD_OPTS) $(FIRMWARE_OPTS)

TOOLDIR = /opt/cross/arm-none-eabi-x-tools/bin/
CC      = $(TOOLDIR)arm-none-eabi-g++
CXX	= $(CC)
AS      = $(CC)
LD      = $(CC)
AR      = $(TOOLDIR)arm-none-eabi-ar
OBJCOPY = $(TOOLDIR)arm-none-eabi-objcopy
CFLAGS  = $(COMPILE_OPTS)
CXXFLAGS= $(COMPILE_OPTS)
ASFLAGS = -x assembler-with-cpp -c $(TARGET_ARCH) $(COMPILE_OPTS) 
LDFLAGS = -Wl,--gc-sections,-Map=bin/main.map,-cref -T stm32_flash.ld $(INCLUDE_DIRS) -lstdc++ -L $(TOOLDIR)../arm-none-eabi/lib/thumb -L ../../Libraries -nostartfiles

all: libstm32f4xx startup bin/main.hex

# main.o is compiled by suffix rule automatucally
bin/main.hex: $(patsubst %.c,%.o,$(wildcard *.c)) $(patsubst %.cpp,%.o,$(wildcard *.cpp)) $(STARTUP_DIR)startup_stm32f4xx.o ../../Libraries/libstm32f4xx.a 
	$(LD) $(LDFLAGS) $(TARGET_ARCH) $^ -o bin/main.elf 
	$(OBJCOPY) -O ihex bin/main.elf bin/main.hex

# many of xxx.o are compiled by suffix rule automatically
LIB_OBJS = $(sort \
 $(patsubst %.c,%.o,$(wildcard ../../Libraries/STM32F4xx_StdPeriph_Driver/src/*.c)))

libstm32f4xx: $(LIB_OBJS)
	$(AR) cr ../../Libraries/libstm32f4xx.a $(LIB_OBJS)
	
startup:
	$(AS) -o $(STARTUP_DIR)/startup_stm32f4xx.o $(ASFLAGS) $(STARTUP_DIR)startup_stm32f4xx.s

$(LIB_OBJS): \
 $(wildcard ../../Libraries/STM32F4xx_StdPeriph_Driver/inc/*.h) \
 $(wildcard ../../Libraries/STM32F4xx_StdPeriph_Driver/src/*.c) \

clean:
	rm -rf *.o *.s bin\*
	
debug:

#flash:
#	start /WAIT C:\"Program Files (x86)"\STMicroelectronics\Software\"Flash Loader Demonstrator"\STMFlashLoader.exe -c --pn 10 --br 115200 --to 5000 -i STM32F4_1024K -e --sec 7 0 1 2 3 4 5 6 -d --fn bin\main.hex
#	start C:\"Program Files (x86)"\teraterm\ttermpro.exe /C=10 /BAUD=921600
